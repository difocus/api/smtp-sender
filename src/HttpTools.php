<?php

namespace ShopExpress\SmtpSender;

class HttpTools
{
    /**
     * @return string
     */
    public static function getHost()
    {
        return $_SERVER['HTTP_HOST'];
    }

    /**
     * @return string
     */
    public static function getScheme(): string
    {
        if ((! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
            || (! empty($_SERVER['HTTP_X_FORWARDED_PROTO'])
                && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
        ) {
            $scheme = 'https';
        } else {
            $scheme = 'http';
        }

        return $scheme;
    }
}