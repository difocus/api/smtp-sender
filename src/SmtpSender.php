<?php

namespace ShopExpress\SmtpSender;

use Psr\Log\LoggerInterface;
use RuntimeException;
use ShopExpress\SmtpSender\Config\MailConfigurationInterface;
use ShopExpress\SmtpSender\Exception\FailedLoginException;
use ShopExpress\SmtpSender\Exception\FailedSendException;
use ShopExpress\SmtpSender\Exception\UnreachableException;

/**
 * Class SmtpSender
 * @package ShopExpress\SmtpSender
 */
final class SmtpSender
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var MailConfigurationInterface
     */
    private $config;
    /**
     * @var null|resource
     */
    private $sock = null;
    /**
     * @var int
     */
    private $timeout = 10;

    /**
     * SmtpSender constructor.
     *
     * @param MailConfigurationInterface $config
     * @param LoggerInterface $logger
     *
     * @throws FailedLoginException
     * @throws UnreachableException
     */
    public function __construct(MailConfigurationInterface $config, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->config = $config;

        $this->initialize();
    }

    /**
     * @throws FailedLoginException
     * @throws UnreachableException
     */
    private function initialize()
    {
        try {
            $this->connect();
            $this->streamSetTimeout();
        } catch (UnreachableException $e) {
            $this->logger->warning('UnreachableException ' . $e->getMessage());
            throw $e;
        } catch (FailedLoginException $e) {
            $this->logger->warning('FailedLoginException' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * @param array $ArrayHeaders
     *
     * @return string
     */
    private function getHeaders(array $ArrayHeaders)
    {
        $headers = '';

        if (!isset($ArrayHeaders['From'])) {
            if (is_string($this->config->getFromName()) && mb_strlen($this->config->getFromName()) > 0) {
                $headers .= 'From: ' . $this->config->getFromName() . ' <' . $this->config->getFromEmail() . ">\r\n";
            } else {
                $headers .= "From: " . $this->config->getFromEmail() . "\r\n";
            }
        }

        $domain = substr($this->config->getFromEmail(), strpos($this->config->getFromEmail(), '@') + 1);
        $headers .= "X-Mailer: ShopExpress mail system, https://$domain\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        foreach ($ArrayHeaders as $header_key => $header_value) {
            $headers .= $header_key . ": " . $header_value . "\r\n";
        }
        return $headers;
    }

    /**
     * @param $receiver
     * @param $subject
     * @param $content
     * @param array $ArrayHeaders
     *
     * @throws FailedLoginException
     * @throws FailedSendException
     * @throws UnreachableException
     * @return $this
     */
    public function send($receiver, $subject, $content, array $ArrayHeaders = [])
    {
        $addressSplit = preg_split('~[,;]~', mb_strtolower($receiver));

        if (sizeof($addressSplit) > 1)
            for ($i = 0; $i < sizeof($addressSplit); $i++) {
                $addressSplit[$i] = trim($addressSplit[$i]);
                $addressSplit[$i] = rtrim($addressSplit[$i]);
                if (!$addressSplit[$i]) continue;

                $this->sendEmail($addressSplit[$i], $subject, $content, $ArrayHeaders);
            }
        else if (trim($receiver)) {
            $this->sendEmail($receiver, $subject, $content, $ArrayHeaders);
        }

        return $this;
    }

    /**
     * @param string $receiver
     * @param string $subject
     * @param $content
     * @param array $ArrayHeaders
     *
     * @throws FailedLoginException
     * @throws UnreachableException
     * @throws FailedSendException
     * @return $this
     */
    private function sendEmail(string $receiver, string $subject, $content, array $ArrayHeaders)
    {
        if ($receiver && !preg_match("/^[_a-z0-9\-\+]+(\.[_a-z0-9\-\+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $receiver)) {
            $this->logger->info('Почтовый адрес "'.$receiver.'" не корректный');
            return $this;
        }

        try {
            $headers = $this->getHeaders($ArrayHeaders);
            $headers .= "Subject: " . $subject . "\r\n";
            $headers .= "To: <$receiver>" . "\r\n";

            $this
                ->sendToSocket("MAIL FROM: <" . $this->config->getFromEmail() . ">")
                ->sendToSocket("RCPT TO: <" . $receiver . ">")
                ->sendToSocket("DATA")
                ->sendToSocket($headers . "\r\n" . $content . "\r\n.");
        } catch (RuntimeException $e) {
            $this->logger->warning($e->getMessage());
            fclose($this->sock);
            $this->initialize();

            throw new FailedSendException($e->getMessage());
        }

        return $this;
    }

    /**
     * @throws RuntimeException
     */
    public function close()
    {
        $this->sendToSocket("QUIT");
        fclose($this->sock);
    }

    public function __destruct()
    {
        try {
            $this->close();
        } catch (RuntimeException $e) {
        }
    }

    /**
     * @throws FailedLoginException
     * @throws UnreachableException
     */
    private function connect()
    {
        $this->sock = fsockopen($this->config->getSmtpServer(), $this->config->getSmtpPort(), $errno, $errstr, 45);

        if (!$this->sock) {
            $this->logger->error("Can't open socket {$this->config->getSmtpServer()} :{$this->config->getSmtpPort()}");
            $this->logger->error("fsockopen:" . $errstr);
            $this->logger->error($this->config->getSmtpLogin() . '@' . $this->config->getSmtpPassword() . '>' . $this->config->getSmtpServer() . ":" . $this->config->getSmtpPort());
            throw new UnreachableException('Socket is not created');
        }

        try {
            $ip = gethostbyname(gethostname());
            $this->sendToSocket("HELO " . $ip);
            if ($this->config->getSmtpLogin()) {
                $this->sendToSocket("AUTH LOGIN");
                $this->sendToSocket(base64_encode($this->config->getSmtpLogin()));
                $this->sendToSocket(base64_encode($this->config->getSmtpPassword()));
            }
        } catch (RuntimeException $e) {
            throw new FailedLoginException($e->getMessage());
        }
    }

    /**
     * @return bool
     */
    protected function streamSetTimeout()
    {
        return stream_set_timeout($this->sock, floor($this->timeout));
    }

    /**
     * @param $msg
     *
     * @throws RuntimeException
     * @return $this
     */
    private function sendToSocket($msg)
    {
        if (!$this->sock) {
            throw new RuntimeException("Socket is down");
        }

        $chunk = @fwrite($this->sock, "$msg\r\n");
        if ($chunk === false) {
            throw new RuntimeException("Could not write to socket");
        }
        $str = @fgets($this->sock, 4096);

        $socketInfo = stream_get_meta_data($this->sock);
        if ($socketInfo['timed_out']) {
            throw new RuntimeException("Read\Write timed-out");
        }

        if (!$this->sock) {
            throw new RuntimeException('Socket is down');
        }

        $e = explode(" ", $str);
        $code = array_shift($e);

        if ($code > 420) {
            $str = implode(" ", $e);
            throw new RuntimeException($str, $code);
        }

        return $this;
    }
}