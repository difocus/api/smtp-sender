<?php

namespace ShopExpress\SmtpSender\Config;

use RuntimeException;

class AbstractConfiguration
{
    /**
     * Required parameters.
     * @var array
     */
    protected $required = [];

    const EXCEPTION_REQUIRED_VIOLATION = 'Parameter `%s` can\'t be empty';

    /**
     * Sets configuration from array, validates required parameters.
     * @param array $params
     * @throws RuntimeException
     */
    public function fromArray(array $params)
    {
        foreach ($this->required as $paramName) {
            if (empty($params[$paramName])) {
                throw new RuntimeException(sprintf(static::EXCEPTION_REQUIRED_VIOLATION, $paramName));
            }
            $methodName = 'set' . ucfirst($paramName);
            call_user_func([$this, $methodName], $params[$paramName]);
        }

        foreach ($params as $key => $value) {
            if (in_array($key, $this->required)) continue;

            $methodName = 'set' . ucfirst($key);
            if (!method_exists($this, $methodName)) continue;
            call_user_func([$this, $methodName], $value);
        }
    }
}