<?php

namespace ShopExpress\SmtpSender\Config;

interface MailConfigurationInterface
{
    /**
     * @return string
     */
    public function getSmtpServer(): string;

    /**
     * @return int
     */
    public function getSmtpPort(): int;

    /**
     * MailConfigurationInterface constructor.
     * @param string|null $smtpServer
     * @param int|null $smtpPort
     * @param string|null $smtpLogin
     * @param string|null $smtpPassword
     * @param string|null $fromEmail
     * @param string|null $fromName
     */
    public function __construct(?string $smtpServer = null, ?int $smtpPort = null, ?string $smtpLogin = null, ?string $smtpPassword = null, ?string $fromEmail = null, ?string $fromName = null);

    /**
     * @return string
     */
    public function getSmtpLogin(): string;

    /**
     * @return string
     */
    public function getSmtpPassword(): string;

    /**
     * @return string|null
     */
    public function getFromName(): ?string;

    /**
     * @return string
     */
    public function getFromEmail(): string;
}