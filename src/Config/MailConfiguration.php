<?php

namespace ShopExpress\SmtpSender\Config;

use Exception;

class MailConfiguration extends AbstractConfiguration implements MailConfigurationInterface
{
    /**
     * @var bool
     */
    protected $useDefaults;

    /**
     * @return string
     */
    public function getSmtpServer(): string
    {
        return $this->smtpServer;
    }

    /**
     * @param string $smtpServer
     */
    protected function setSmtpServer(string $smtpServer): void
    {
        $this->smtpServer = $smtpServer;
    }

    /**
     * @return int
     */
    public function getSmtpPort(): int
    {
        return $this->smtpPort;
    }

    /**
     * @param int $smtpPort
     */
    protected function setSmtpPort(int $smtpPort): void
    {
        $this->smtpPort = $smtpPort;
    }

    /**
     * @return string
     */
    public function getSmtpLogin(): string
    {
        return $this->smtpLogin;
    }

    /**
     * @param string $smtpLogin
     */
    protected function setSmtpLogin(string $smtpLogin): void
    {
        $this->smtpLogin = $smtpLogin;
    }

    /**
     * @return string
     */
    public function getSmtpPassword(): string
    {
        return $this->smtpPassword;
    }

    /**
     * @param string $smtpPassword
     */
    protected function setSmtpPassword(string $smtpPassword): void
    {
        $this->smtpPassword = $smtpPassword;
    }

    /**
     * @return string|null
     */
    public function getFromName(): ?string
    {
        return $this->fromName;
    }

    /**
     * @param string|null $fromName
     */
    protected function setFromName(?string $fromName): void
    {
        $this->fromName = $fromName;
    }

    /**
     * @return string
     */
    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    /**
     * @param string $fromEmail
     */
    protected function setFromEmail(string $fromEmail): void
    {
        $this->fromEmail = $fromEmail;
    }

    /** @var string */
    private $smtpServer;

    /** @var int */
    private $smtpPort;

    /** @var string */
    private $smtpLogin;

    /** @var string */
    private $smtpPassword;

    /** @var string|null */
    private $fromName;

    /** @var string */
    private $fromEmail;

    protected $required = [
        'smtpServer',
        'smtpPort',
        'smtpLogin',
        'smtpPassword',
        'fromEmail'
    ];

    /**
     * MailConfiguration constructor.
     * @param string|null $smtpServer
     * @param int|null $smtpPort
     * @param string|null $smtpLogin
     * @param string|null $smtpPassword
     * @param string|null $fromEmail
     * @param string|null $fromName
     * @throws Exception
     */
    public function __construct(?string $smtpServer = null, ?int $smtpPort = null, ?string $smtpLogin = null, ?string $smtpPassword = null, ?string $fromEmail = null, ?string $fromName = null)
    {
        if (!empty($fromName)) {
            $this->fromName = $fromName;
        }

        if (!empty($fromEmail)) {
            $this->fromEmail = $fromEmail;
        } else {
            throw new Exception(sprintf(static::EXCEPTION_REQUIRED_VIOLATION, 'fromEmail'));
        }

        if (!empty($smtpServer)) {
            $this->smtpServer = $smtpServer;
        } else {
            throw new Exception(sprintf(static::EXCEPTION_REQUIRED_VIOLATION, 'smtpServer'));
        }

        if (!empty($smtpPort)) {
            $this->smtpPort = $smtpPort;
        } else {
            throw new Exception(sprintf(static::EXCEPTION_REQUIRED_VIOLATION, 'smtpPort'));
        }

        $this->smtpLogin = $smtpLogin;
        $this->smtpPassword = $smtpPassword;
    }
}