<?php
require 'vendor/autoload.php';

use Psr\Log\NullLogger;
use ShopExpress\SmtpSender\Config\MailConfiguration;
use ShopExpress\SmtpSender\SmtpSender;

try {
    /*
        self::$GLOB_VAR['smtp_server'] = 'smtp.mailgun.org';
        self::$GLOB_VAR['smtp_port'] = '25';
        self::$GLOB_VAR['smtp_user'] = 'postmaster@difocus.ru';
        self::$GLOB_VAR['smtp_password'] = '7c92ccdaa5abe4d4e915e41b911c8cad';

     */
    $config = new MailConfiguration(
        'tls://smtp.yandex.ru',
        465,
        'bar@yandex.ru',
        'baz',
        'bar@yandex.ru',
        'Difocus'
    );

    $mailTo = new SmtpSender($config, new NullLogger());
    $mailTo->send('grandz0d@gmail.com', 'Пример темы', 'Пример контента', []);
} catch (Throwable $e) {
    echo $e->getTraceAsString() . PHP_EOL;
    echo $e->getMessage();
}