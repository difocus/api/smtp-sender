<?php

namespace ShopExpress\SmtpSender\Tests;


use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use ShopExpress\SmtpSender\Config\MailConfiguration;
use ShopExpress\SmtpSender\SmtpSender;

class TestSmtpSender extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testConfig()
    {
        $config = parse_ini_file(__DIR__ . '/../.env');
        $mailConfiguration = new MailConfiguration(
            $config['smtp_server'],
            $config['smtp_port'],
            $config['smtp_user'],
            $config['smtp_password'],
            $config['from']
        );
        $this->assertIsObject($mailConfiguration);

        return $mailConfiguration;
    }

    /**
     * @depends testConfig
     * @param MailConfiguration $mailConfiguration
     * @return SmtpSender
     */
    public function testConnect(MailConfiguration $mailConfiguration)
    {
        $sender = new SmtpSender($mailConfiguration, new NullLogger());
        $this->assertIsObject($sender);
        return $sender;
    }

    /**
     * @depends testConnect
     * @param SmtpSender $sender
     * @return void
     * @throws Exception
     */
    public function testSendMail(SmtpSender $sender)
    {
        $sender
            ->send(
                'test+1@difocus.ru',
                'тестовое письмо',
                'Это текст тестового письма'
            )
            ->send(
                'test+2@difocus.ru',
                'тестовое письмо',
                'Это текст тестового письма'
            )
            ->send(
                'test+3@difocus.ru',
                'тестовое письмо',
                'Это текст тестового письма'
            );
        $this->assertIsObject($sender);
    }


    /**
     * @depends testConnect
     * @param SmtpSender $sender
     * @return void
     * @throws Exception
     */
    public function testAutoReconnect(SmtpSender $sender)
    {
        $sender
            ->send(
                'asdlaksdklhalskldas',
                'тестовое письмо с неверным адресом',
                'Это текст тестового письма'
            )
            ->send(
                'test+4@difocus.ru',
                'тестовое письмо после письма с неверным адресом',
                'Это текст тестового письма'
            );

        $this->assertIsObject($sender);
    }
}